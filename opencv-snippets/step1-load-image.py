import os
import cv2

def checkCurrentPath():
    path = os.getcwd()
    if path.endswith("wlml-opencv"):
        path = path + "/opencv-snippets"
    if not path.endswith("opencv-snippets"):
        print("Please start this snippet from root directory of this repository or from 'opencv-snippets' directory")
        quit()
    return path

path = checkCurrentPath()

img = cv2.imread(path + "/05.jpg")

cv2.imshow("My image", img)

while True:
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.imwrite(path + "/out/saved-image.jpg", img)
cv2.destroyAllWindows()
