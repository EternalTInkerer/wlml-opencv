import os
import cv2

def checkCurrentPath():
    path = os.getcwd()
    if path.endswith("wlml-opencv"):
        path = path + "/opencv-snippets"
    if not path.endswith("opencv-snippets"):
        print("Please start this snippet from root directory of this repository or from 'opencv-snippets' directory")
        return False
    return path

path = checkCurrentPath()
if path != False:
    img = cv2.imread("05.jpg")

    # Kernel definition
    kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(24,24))

    # Blur
    res = cv2.medianBlur(img, 11)

    # Threshold on color image
    res = cv2.threshold(res, 20, 255, cv2.THRESH_BINARY)[1]

    # Erosion
    res = cv2.morphologyEx(res, cv2.MORPH_ERODE, kernel1)

    # Convert to gray
    gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
    # Blur again
    gray = cv2.medianBlur(gray, 21)

    # Threshold to Black & White
    black = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY)[1]

    cv2.imshow("Original image", img)
    cv2.imshow("Thresholded image", res)
    cv2.imshow("B&W image", black)

    while True:
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()