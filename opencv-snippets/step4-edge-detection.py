import os
import cv2
import numpy as np

def checkCurrentPath():
    path = os.getcwd()
    if path.endswith("wlml-opencv"):
        path = path + "/opencv-snippets"
    if not path.endswith("opencv-snippets"):
        print("Please start this snippet from root directory of this repository or from 'opencv-snippets' directory")
        quit()
    return path

path = checkCurrentPath()

img = cv2.imread(path + "/05.jpg")

# Kernel definition
kernel0 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(6,6))
kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(24,24))
kernel2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(64,64))
kernel3 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(10,10))

# Converts images from BGR to HSV 
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

# Edge detection Algo 1
algo1 = cv2.Canny(hsv, 145, 180)
algo1 = cv2.medianBlur(algo1, 3)
algo1 = cv2.morphologyEx(algo1, cv2.MORPH_CLOSE, kernel0, iterations = 2)
# Background area using Dilation 
algo1 = cv2.medianBlur(algo1, 3)
algo1 = cv2.dilate(algo1, kernel0, iterations = 2)

# Edge detection Algo 2
# Defines color range to filter out blue hue
lower_red = np.array([100,70,70])
upper_red = np.array([130,255,255])
algo2 = cv2.inRange(hsv, lower_red, upper_red)
algo2 = cv2.morphologyEx(algo2, cv2.MORPH_DILATE, kernel1, iterations = 5)

# Apply mask to original image
merge = cv2.bitwise_and(algo1 ,algo1, mask=algo2)

# Apply edge detection
edges = cv2.Canny(merge, 50, 100)
edges = cv2.bitwise_not(edges)
edges = cv2.bitwise_and(img ,img, mask=edges)

cv2.imshow("Algo 1", algo1)
cv2.imshow("Algo 2", algo2)
cv2.imshow("Algo 1 + Algo 2", merge)
cv2.imshow("Edges", edges)

while True:
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
