#!/bin/sh
#cp ./cfg/yolov3-$1.cfg ../../darknet/custom
#cp ./data/drone* ../../darknet/custom
cd ../../darknet
./darknet detector train custom/drone.data custom/yolov3$1.cfg darknet53.conv.74 -gpus 0,1
