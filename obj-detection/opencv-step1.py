import os
import cv2
import argparse
import numpy as np

# Initialize the parameters
confThreshold = 0.5  #Confidence threshold
nmsThreshold = 0.4   #Non-maximum suppression threshold
inpWidth = 416       #Width of network's input image
inpHeight = 416      #Height of network's input image

# Give the configuration and weight files for the model and load the network using them.
modelConfiguration = "obj-detection/cfg/yolov3.cfg"
modelWeights = "obj-detection/yolov3.weights"

# Load names of classes
classesFile = "obj-detection/data/coco.names"
classes = None

with open(classesFile, 'rt') as f:
    classes = f.read().rstrip('\n').split('\n')

outputFile = "obj-detection/out/yolo_out_py.avi"

# Commandline arguments parsing
parser = argparse.ArgumentParser(description='...')
parser.add_argument('--image', default=False, type=str, help="Image")
parser.add_argument('--video', default=False, type=bool, help="write video to ./out folder")

args = parser.parse_args()

writeVideo = args.video
if (args.image):
    # Open the image file
    if not os.path.isfile(args.image):
        print("Input image file ", args.image, " doesn't exist")
        sys.exit(1)
    cap = cv2.VideoCapture(args.image)
    outputFile = './out/' + args.image[:-4]+'_yolo_out_py.jpg'
else:
    # Webcam input
    cap = cv2.VideoCapture(0)
 
# Get the video writer initialized to save the output video
if (writeVideo):
    vid_writer = cv2.VideoWriter(outputFile, cv2.VideoWriter_fourcc('M','J','P','G'), 30, (round(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),round(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))

while cv2.waitKey(1) < 0:
    # get frame from the video
    hasFrame, frame = cap.read()
     
    # Stop the program if reached end of video
    if not hasFrame:
        print("Done processing !!!")
        print("Output file is stored as ", outputFile)
        cv2.waitKey(3000)
        break
 
    # Write the frame with the detection boxes
    if (args.image):
        cv2.imwrite(outputFile, frame.astype(np.uint8));
    else:
        if writeVideo:
            vid_writer.write(frame.astype(np.uint8))
        else:
            cv2.imshow('Final',frame)

cap.release()
if writeVideo:
    vid_writer.release()
cv2.destroyAllWindows()

