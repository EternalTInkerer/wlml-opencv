# OpenCV & Object detection Lab

## Contents

- [OpenCV & Object detection Lab](#opencv--object-detection-lab)
  - [Contents](#contents)
  - [OpenCV Introduction](#opencv-introduction)
    - [Install OpenCV](#install-opencv)
    - [Code snippets](#code-snippets)
  - [Object detection](#object-detection)
    - [Machine Setup](#machine-setup)
      - [labelImg](#labelimg)
      - [Darknet installation](#darknet-installation)
      - [Python packages](#python-packages)
      - [CUDA and cuDNN](#cuda-and-cudnn)
      - [Downloads](#downloads)
    - [Image labelling](#image-labelling)
    - [Train Darknet](#train-darknet)
    - [Detect drones](#detect-drones)
      - [Step 1](#step-1)
      - [Step 2](#step-2)
      - [Step 3](#step-3)
      - [Step 4](#step-4)
  - [Links](#links)

## OpenCV Introduction

### Install OpenCV

Install [OpenCV](https://opencv.org) and other needed libraries by opening your commandline and typing following:

PIP

```cmd
pip3 install opencv numpy
```

Anaconda

```cmd
conda install opencv numpy
```

### Code snippets

We will discuss basic functions to get a bit of understanding how OpenCV works.

- __Step 1__
  - Load/Save image - [source](blob/master/opencv-snippets/step1-load-image.py)
- __Step 2__
  - Apply blur - [source](blob/master/opencv-snippets/step2-apply-blur.py)
- __Step 3__
  - Apply Threshold - [source](blob/master/opencv-snippets/step3-threshold.py)
- __Step 4__
  - Edge detection - [source](blob/master/opencv-snippets/step4-edge-detection.py)

## Object detection

In this section we will learn more how to setup __You Only Look Once__ (YoLo) detection and how to train it using [Darknet AlexeyAB fork](https://github.com/AlexeyAB/darknet) model on your own images.

- [Yolo V3 Paper (PDF)](https://pjreddie.com/media/files/papers/YOLOv3.pdf)

### Machine Setup

#### labelImg

Download and install __labelImg__ software to classify images for training. 

#### Darknet installation

Install Darknet only if you perform step 3 - Training. It is also advised to have CUDA capable gfx card on your computer. On Linux use following script:

```bash
$> ./install-darknet.sh
```

additional details about installation can be found [here](https://pjreddie.com/darknet/install/)

#### Python packages

PIP

```bash
$> pip3 install matplotlib pandas
$> pip3 install pytorch torchvision cudatoolkit=9.0 -c pytorch
```

Anaconda

```bash
$> conda install matplotlib pandas
$> conda install pytorch torchvision cudatoolkit=9.0 -c pytorch
```

#### CUDA and cuDNN

Please 
If you have Nvidia CUDA capable graphics card, please install latest drivers and CUDA packages. In order to install cuDNN navigate to this [page](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html) for further instructions.

#### Downloads

- [yolov3.weights](https://pjreddie.com/media/files/yolov3.weights)
- [yolov3-tiny.weights](https://pjreddie.com/media/files/yolov3-tiny.weights)
- [Darknet53.conv.74](http://pjreddie.com/media/files/darknet53.conv.74)
- [labelImg Windows](https://www.dropbox.com/s/kqoxr10l3rkstqd/windows_v1.8.0.zip?dl=1)
- [labelImg Linux](https://www.dropbox.com/s/bpbo3oh0zgl0522/linux_v1.4.3.zip?dl=1)
- [cuDNN](https://developer.nvidia.com/rdp/cudnn-download)

### Image labelling

Can be done in two programs, take your pick. There is [labelImg](https://github.com/tzutalin/labelImg) tool and [Yolo_mark](https://github.com/AlexeyAB/Yolo_mark). Both programs will allow you to label images by drawing a rectangular area.

First create a file named 'drone.names' and enter a name of your class, one per line.

```bash
person
drone
```

In this case __person__ has index 0 and __drone__ 1. For convenience example file is [here](blob/master/obj-detection/data/drone.names)
Now start labeling program of your choice and provide it with file above.

Open directory obj-detection/img and load images. Then select an image and create region which you label as __drone__ or __person__.
Resulting files will be having same name as your images but with extension .txt containing following representation:

```
<object-class> <x_center> <y_center> <width> <height>

1 0.716797 0.395833 0.216406 0.147222
0 0.687109 0.379167 0.255469 0.158333
```

last step is to partition data into __training set__ and __testing set__. Therefore we have script named [partition-data.py](blob/master/obj-detection/partition-data.py) which does exactly that. So run it in its directory and it will produce two files in ./data directory:

```bash
drones-train.txt
drones-test.txt
```

Example of interface labelImg

![Label drone in image](img/labelling.jpg)

### Train Darknet

After you have [installed Darknet](#darknet-installation) and prepared images with labeling software in previous section we can perform training.

We will start by creating our own directory named __custom__ in darknet folder. Then we need to copy YoLo configuration file [yolov3.cfg](blob/master/obj-detection/cfg/yolov3.cfg) or [yolov3-tiny.cfg](blob/master/obj-detection/cfg/yolov3-tiny.cfg) and name it differently (eg: yolov3-drones.cfg).
Now copy all files that begin with 'drone' from your [./data](tree/master/obj-detection/data) directory to newly created 'custom' directory. You shall end up with something like this:

```bash
directory: darknet/custom/
 ..
 drone.data
 drone.names
 drone-test.txt
 drone-train.txt
 yolov3-drones.cfg
```

Now we need to copy our images and their labeling files into this directory as well. After copy it should look like this:

```bash
directory: darknet/custom/
 ..
 img
  01.jpg
  01.txt
  02.jpg
  02.txt
  ...
  243.jpg
  243.txt
 drone.data
 drone.names
 drone-test.txt
 drone-train.txt
 yolov3-drones.cfg
```

Let's configure our definitions for learning (how many classes we have and where other config files are stored). Open file [drone.data](blob/master/obj-detection/data/drone.data) and verify paths. If you kept above layout it should look like this:

```bash
imgclasses= 2
train  = custom/drone-train.txt
valid  = custom/drone-test.txt
names = custom/drone.names
backup = backup/
```

Finally we can proceed to model definition. Open copied file yolov3-drones.cfg in your favorite text editor and edit following lines on top of file:

```bash
batch=64
subdivisions=8
max_batches = 4096
```

Search for [yolo] sections and in [convolutional] section above [yolo] section change filters to value calculated by following formula:

```bash
filters = (classes + 5) * 3
```

If you have two classes like we do then result is following:

```bash
filters = (2 + 5) * 3 = 21
```

so adjusted section should look like:

```bash
[convolutional]
size=1
stride=1
pad=1
filters=21
activation=linear
```

once we save this we are ready to train.

If you are on Linux, run this in darknet root folder:

```bash
./darknet detector train custom/drone.data custom/yolov3-drones.cfg darknet53.conv.74
```

or if you have multiple Nvidia GPUs

```bash
./darknet detector train custom/drone.data custom/yolov3-drones.cfg darknet53.conv.74 -gpus 0,1
```

NOW GO AND GET SOME DRINK, BECAUSE IT IS GONNA TAKE LONG SWEET TIME!!!

### Detect drones 

#### Step 1

In this section we will reuse our acquired knowledge of OpenCV and perform object detection in realtime. To begin with open file [opencv-step1.py](blob/master/obj-detection/opencv-step1.py). Here we will implement commandline argument parsing, video acquiring from webcam and eventual video writing.

#### Step 2

Let's add reading of our data files and processing
[opencv-step2.py](blob/master/obj-detection/opencv-step2.py)

#### Step 3

We will implement here bounding boxes display and finalize whole script.

#### Step 4

Here we will change our weights and use newly trained model to see if it works.

## Links

- [Pascal VOC Challenge](http://host.robots.ox.ac.uk/pascal/VOC/)
- [PyTorch Models](https://pytorch.org/docs/master/torchvision/models.html)
- [DroneFace Open Dataset](https://www.iis.sinica.edu.tw/~swc/pub/drone_face_open_dataset.html)
- [Yolo with Movidius](https://github.com/duangenquan/YoloV2NCS)
